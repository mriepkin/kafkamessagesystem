from kafka_message_system.abstract_classes import AbstractConsumer
from kafka_message_system.configs.consumer_config import ConsumerConfig
from kafka import KafkaConsumer as Consumer


class KafkaConsumer(AbstractConsumer):
    def __init__(self, consumer_config: ConsumerConfig):
        self._listening_topic = consumer_config.topic
        self._consumer = Consumer(consumer_config.topic
                                  , bootstrap_servers=consumer_config.server
                                  , auto_offset_reset=consumer_config.auto_offset_reset
                                  , enable_auto_commit=consumer_config.enable_auto_commit
                                  , auto_commit_interval_ms=consumer_config.auto_commit_interval_ms
                                  , group_id=consumer_config.group_id
                                  , value_deserializer=consumer_config.value_serializer)

    def consume_message(self, time_to_wait: int = 1500) -> dict:
        topic_partitions = self._consumer.poll(time_to_wait)
        return topic_partitions

    def close_connections(self):
        self._consumer.close()

    def get_listening_topic(self):
        return self._listening_topic
