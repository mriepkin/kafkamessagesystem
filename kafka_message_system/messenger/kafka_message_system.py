from kafka_message_system.abstract_classes.abstract_messaging_system import AbstractMessagingSystem
from kafka_message_system.messenger.kafka_consumer import KafkaConsumer
from kafka_message_system.messenger.kafka_producer import KafkaProducer
from kafka_message_system.configs.kafka_config import KafkaConfig


class KafkaMessageSystem(AbstractMessagingSystem):

    def __init__(self, config: KafkaConfig):
        super(KafkaMessageSystem, self).__init__()
        self._producer = KafkaProducer(config.producer)
        self._consumer = KafkaConsumer(config.consumer)
        KafkaMessageSystem.__instance = self

    def consume_message(self) -> dict:
        return self._consumer.consume_message()

    def produce_message(self, topic, message):
        self._producer.produce_message(topic, message)

    def flush_messages(self):
        self._producer.flush_messages()

    def close_connections(self):
        self._producer.close_connections()
        self._consumer.close_connections()

    def get_listening_topic(self):
        return self._consumer.get_listening_topic()