from kafka_message_system.abstract_classes import AbstractProducer
from kafka_message_system.configs.producer_config import ProducerConfig
from kafka import KafkaProducer as Producer


class KafkaProducer(AbstractProducer):
    def __init__(self, producer_config: ProducerConfig):
        self._producer = Producer(bootstrap_servers=producer_config.server
                                       , value_serializer=producer_config.value_serializer)

    def produce_message(self, topic, message):
        self._producer.send(topic, message)

    def flush_messages(self):
        self._producer.flush()

    def close_connections(self):
        self._producer.close()
