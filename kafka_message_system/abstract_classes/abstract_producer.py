from abc import ABC, abstractmethod


class AbstractProducer(ABC):

    @abstractmethod
    def produce_message(self, topic, message):
        pass

    @abstractmethod
    def flush_messages(self):
        pass

    @abstractmethod
    def close_connections(self):
        pass
