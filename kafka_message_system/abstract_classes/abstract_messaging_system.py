from abc import ABC, abstractmethod


class AbstractMessagingSystem(ABC):

    @abstractmethod
    def produce_message(self, topic, message):
        pass

    @abstractmethod
    def consume_message(self):
        pass

    @abstractmethod
    def close_connections(self):
        pass

    @abstractmethod
    def get_listening_topic(self):
        pass

    @abstractmethod
    def flush_messages(self):
        pass
