from abc import ABC, abstractmethod


class AbstractConsumer(ABC):

    @abstractmethod
    def consume_message(self, time_to_wait: int):
        pass

    @abstractmethod
    def get_listening_topic(self):
        pass

    @abstractmethod
    def close_connections(self):
        pass
