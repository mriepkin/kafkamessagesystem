from typing import List


class ProducerConfig:
    def __init__(self, server: List[str], value_serializer):
        self.server = server
        self.value_serializer = value_serializer
