from kafka_message_system.configs.consumer_config import ConsumerConfig
from kafka_message_system.configs.producer_config import ProducerConfig


class KafkaConfig:
    def __init__(self, producer: ProducerConfig, consumer: ConsumerConfig):
        self.producer = producer
        self.consumer = consumer
