from typing import List


class ConsumerConfig:
    def __init__(self,
                 topic: str,
                 server: List[str],
                 value_serializer,
                 enable_auto_commit: bool = True,
                 auto_offset_reset: str = 'earliest',
                 auto_commit_interval_ms: int = 1000,
                 group_id: str = 'counters'):
        self.topic = topic
        self.server = server
        self.enable_auto_commit = enable_auto_commit
        self.auto_offset_reset = auto_offset_reset
        self.auto_commit_interval_ms = auto_commit_interval_ms
        self.group_id = group_id
        self.value_serializer = value_serializer
