import json

PRODUCER_SERIALIZE_FUNCTION = lambda x: json.dumps(x).encode('utf-8')
CONSUMER_DESERIALIZE_FUNCTION = lambda x: json.loads(x.decode('utf-8'))
