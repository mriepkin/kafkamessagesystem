#from distutils.core import setup
import setuptools

with open('requirements.txt') as f:
    required = f.read().splitlines()

setuptools.setup(
    name="kafka_message_system",
    version="0.34",
    description="This is wrapper over kafka",
    author="Maksym Riepkin",
    author_email="maksym.riepkin@mycredit.ua",
    packages=[
        'kafka_message_system',
        'kafka_message_system.abstract_classes',
        'kafka_message_system.messenger',
        'kafka_message_system.configs',
        'kafka_message_system.utils'
    ],
    install_requires=required,
)

